window.onload = function(){document.getElementById('nim').focus()}

function checkForm(){
  $("#nim").removeClass("invalid");
  var nim = document.getElementById('nim');

  $('#log-form').attr('action', '#');
  $(".showed").addClass("hidden");
  $(".hidden").removeClass("showed");

  if(nim.value.length == 10 && /^\d+$/.test(nim.value)){
    var nim = $('#nim').val();
    var ajax = new XMLHttpRequest();
    var method = "POST";
    var url = "operation/getname.php";
    var async = true;
    ajax.open(method, url, async);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send('nim='+nim);
    ajax.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){
        // alert(this.responseText);
        var data = this.responseText;
        var pass = data != '' ? data.substring(0, 1) : null;
        var fullname = data != '' ? data.substring(1, data.length) : null;
        var name = data != '' ? fullname.split(" ")[0] : null;
        // alert(fullname+' '+name+' '+pass);
        // alert(data);
        if(data != '' && pass == true){
          $('#username').html(name);

          $('#log-form').attr('action', 'operation/login.php');
          $("#login-form").addClass("showed");
          $("#login-form").removeClass("hidden");
          document.getElementById('password-login').focus();

          $("#nama").attr("required", false);
          $("#password-reg").attr("required", false);
          $("#confword-reg").attr("required", false);

          $("#password-login").attr("required", true);
        }else{
          if(data != '' && pass == false){
            $("#nama").val(fullname);
            $("#nama").attr("disabled", true);

            $('#log-form').attr('action', '#');
            $("#register-form").addClass("showed");
            $("#register-form").removeClass("hidden");
            document.getElementById('password-reg').focus();
          }else{
            $("#nama").val(null);
            $("#nama").attr("disabled", false);

            $('#log-form').attr('action', 'operation/register.php');
            $("#register-form").addClass("showed");
            $("#register-form").removeClass("hidden");
            document.getElementById('nama').focus();
          }

          $("#nama").attr("required", true);
          $("#password-reg").attr("required", true);
          $("#confword-reg").attr("required", true);

          $("#password-login").attr("required", false);
        }
      }
    }
  }
  else if(nim.value.length > 0)
    $("#nim").addClass("invalid");

    $("#nama").attr("required", false);
    $("#password-reg").attr("required", false);
    $("#password-login").attr("required", false);
    // nim.focus()
}

function getNama(){
  var nim = $('#nim').val();
  var ajax = new XMLHttpRequest();
  var method = "POST";
  var url = "operation/getname.php";
  var async = false;

  ajax.open(method, url, async);
  ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  ajax.send('nim='+nim);
  var data = '';
  ajax.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      data = this.responseText;
    }
  }
  return String(data);
}
