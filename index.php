<?php
  session_start();
  // session_destroy();
  $msg = isset($_SESSION['message']) ? $_SESSION['message'] : null;
 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Schedule Matcher</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <script type="text/javascript" src="js\js.js" charset="utf-8"></script>

  </head>
  <body>
    <div class="container">
      <h1>schedule matcher</h1>
      <span><?=$msg?></span>
      <form id="log-form" class="" action="#" method="post">
        <input value="" type="text" autocomplete="off" name="nim" id="nim" pattern="[0-9]+.{9}" maxlength="10" onkeyup="checkForm()" placeholder="NIM" value="1202162344" required><br>
        <!-- Register -->
        <div id="register-form" class="hidden">
          <p class="user-alert">Kamu belum terdaftar.<br><span id="msg-reg">Isi form dibawah untuk mendaftar</span></p>
          <input type="text" autocomplete="off" name="nama" id="nama" placeholder="Nama" value="" required><br>
          <!-- <select class="" name="lab" id="lab" required>
            <option value="daspro" selected>DASPRO Laboratory</option>
            <option value="daspro" disabled>EAD Laboratory</option>
            <option value="daspro" disabled>SISJAR Laboratory</option>
            <option value="daspro" disabled>BPAD Laboratory</option>
            <option value="daspro" disabled>ERP Laboratory</option>
            <option value="daspro" disabled>ITGRC Laboratory</option>
          </select><br> -->
          <input type="password" name="password-reg" id="password-reg" placeholder="Passsword" value="" required><br>
          <input type="password" name="confpass-reg" id="confpass-reg" placeholder="Konfirmasi Passsword" required><br>
          <hr>
          <input type="submit" name="" value="Daftar">
        </div>
        <!-- Login -->
        <div id="login-form" class="hidden">
          <div class="user-alert">Hi! <span id="username"></span></div>
          <input type="password" name="password-login" id="password-login" placeholder="Passsword" required><br>
          <hr>
          <input type="submit" name="" value="Login">
          <p id="result"></p>
        </div>
      </form>
    </div>
    <script type="text/javascript" src="js\jquery.min.js" charset="utf-8"></script>
  </body>
</html>

<script>

</script>

<script>
// AJAX REGISTER

// $('#log-form').submit(function(e){
//   e.preventDefault();


// });

</script>
